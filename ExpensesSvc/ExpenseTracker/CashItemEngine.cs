﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExpenseTracker.Business
{
    class CashItemEngine
    {
        private IExpenseQuery _expensesQuery;
        public CashItemEngine(IExpenseQuery expensesQuery)
        {
            this._expensesQuery = expensesQuery;
        }

        public ExpenseCategory[] ExpenseCategories
        {
            get
            {
                return RefDataCache.ExpenseCategories;
            }
        }

        public long CreateExpenseItem(DateTime expenseDate, int categoryId, string notes, double amount)
        {
            return _expensesQuery.UpdateExpenseItem(-1, expenseDate, categoryId, notes, amount);
        }

        public bool TrySaveExpenseItem(long expenseID, DateTime expenseDate, int categoryId, string notes, double amount)
        {
            return (_expensesQuery.UpdateExpenseItem(expenseID, expenseDate, categoryId, notes, amount) == expenseID);
        }

        public bool TryDeleteExpenseItem(long expenseId)
        {
            return _expensesQuery.DeleteExpenseItem(expenseId);
        }

        public string[] GetExpenseItems()
        {
            string[] expenseItemsInJson = null;
            try
            {
                DataSet expenseItems = _expensesQuery.GetExpenseItems();
                expenseItemsInJson = GetExpenseItems(expenseItems);
            }
            catch (Exception ex)
            {
                //do nothing
            }
            return expenseItemsInJson;
        }

        public string [] SearchExpenseItems(DateTime? dateFrom, DateTime? dateTo)
        {
            string[] expenseItemsInJson = null;
            try
            {
                DataSet expenseItems = _expensesQuery.SearchExpenseItems(dateFrom, dateTo);
                expenseItemsInJson = GetExpenseItems(expenseItems);
            }
            catch (Exception ex)
            {
                //do nothing
            }
            return expenseItemsInJson;
        }

        public string SearchExpenseItemById(long expenseId)
        {
            string expenseItemInJson = null;
            try
            {
                DataSet expenseItems = _expensesQuery.SearchExpenseItemById(expenseId);
                var items = GetExpenseItems(expenseItems);
                if(items != null && items.Length == 1)
                {
                    expenseItemInJson = items[0];
                }
            }
            catch (Exception)
            {
                //do nothing
            }
            return expenseItemInJson;
        }

        private string [] GetExpenseItems(DataSet expenseItems)
        {
            string[] expenseItemsInJson = null;
            if (expenseItems != null && expenseItems.Tables.Count > 0)
            {
                DataTable expenseItemsTable = expenseItems.Tables[0];
                var expenseItemsList = Enumerable.ToList<Model.CashItem>(from expenseItemRow in expenseItemsTable.AsEnumerable()
                                                                         select new Model.CashItem()
                                                                         {
                                                                             ExpenseId = expenseItemRow.Field<long>("ExpenseID"),
                                                                             ExpenseDate = expenseItemRow.Field<DateTime>("ExpenseDate"),
                                                                             UserId = expenseItemRow.Field<int>("UserID"),
                                                                             CategoryId = expenseItemRow.Field<int>("CategoryId"),
                                                                             Notes = expenseItemRow.Field<string>("Notes"),
                                                                             Amount = expenseItemRow.Field<double>("Amount"),
                                                                         });


                if (expenseItemsList.Count > 0)
                {
                    expenseItemsInJson = new string[expenseItemsList.Count];
                    for (int index = 0; index < expenseItemsList.Count; index++)
                    {
                        string output = JsonConvert.SerializeObject(expenseItemsList[index]);
                        expenseItemsInJson[index] = output;
                    }
                }
            }
            return expenseItemsInJson;
        }
    }
}

