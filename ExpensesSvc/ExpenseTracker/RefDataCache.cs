﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business
{
    static class RefDataCache
    {
        // Create a logger for use in this class
        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(RefDataCache));

        private static Dictionary<string, ExpenseCategory> _categories = new Dictionary<string, ExpenseCategory>();
        private static ExpenseCategory[] _categoriesList = null;

        private static IExpenseQuery _expensesQuery;

        static RefDataCache()
        {
            _expensesQuery = Factory.GetExpenseQuery();
            Init();
        }
        
        public static ExpenseCategory [] ExpenseCategories
        {
            get
            {
                if(_categoriesList == null)
                    _categoriesList = _categories.Values.ToArray<ExpenseCategory>();
                return _categoriesList;
            }
        }

        private static void Init()
        {
            try
            {
                //Build expense categories
                var categoriesSet = _expensesQuery.FetchCategories();
                if (categoriesSet != null && categoriesSet.Tables.Count == 1 && categoriesSet.Tables[0].Rows.Count > 0)
                {
                    if (log.IsInfoEnabled)
                        log.Info(string.Format("Fetched {0} Expense Categories", categoriesSet.Tables[0].Rows.Count));

                    var catgoriesList = Enumerable.ToList<ExpenseCategory>(categoriesSet.Tables[0].AsEnumerable().Select(
                            categoryRow => new ExpenseCategory()
                            {
                                Id = categoryRow.Field<int>("ID"),
                                CategoryCode = categoryRow.Field<string>("Code"),
                                Description = categoryRow.Field<string>("Description")
                            }
                        ));

                    foreach (var category in catgoriesList)
                        _categories.Add(ExpenseCategory.GetKey(category), category);
                }
                else
                {
                    if (log.IsInfoEnabled)
                        log.Info("Unable to pull expense categories list.");
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.Error(ex);
            }
        }
    
        private static void Clear()
        {
            _categories.Clear();
        }
    }

    public class ExpenseCategory
    {
        public int Id { get; set; }
        public string CategoryCode { get; set; }
        public string Description { get; set; }
        
        public static string GetKey(ExpenseCategory category)
        {
            return string.Format("{0}", category.CategoryCode);
        }
    }
}
