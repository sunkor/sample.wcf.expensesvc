﻿using System;

namespace ExpenseTracker.Business
{
    class ExpenseTracker : IExpenseTracker
    {
        private readonly CashItemEngine _cashItemEngine = null;

        public CashItemEngine Engine {  get { return _cashItemEngine; } }

        public ExpenseTracker(IExpenseQuery query)
        {
            this._cashItemEngine = new CashItemEngine(query);
        }

        public ExpenseCategory[] ExpenseCategories
        {
            get { return Engine.ExpenseCategories; }
        }

        public long CreateExpenseItem(DateTime expenseDate, int categoryId, string notes, double amount)
        {
            return Engine.CreateExpenseItem(expenseDate, categoryId, notes, amount);
        }
        
        public bool TrySaveExpenseItem(long expenseID, DateTime expenseDate, int categoryId, string notes, double amount)
        {
            return Engine.TrySaveExpenseItem(expenseID, expenseDate, categoryId, notes, amount);
        }

        public bool TryDeleteExpenseItem(long expenseId)
        {
            return Engine.TryDeleteExpenseItem(expenseId);
        }

        public string[] GetExpenseItems()
        {
            return Engine.GetExpenseItems();
        }

        public string [] SearchExpenseItems(DateTime? dateFrom, DateTime? dateTo)
        {
            return Engine.SearchExpenseItems(dateFrom, dateTo);
        }
                
        public string SearchExpenseItemById(long expenseId)
        {
            return Engine.SearchExpenseItemById(expenseId);
        }
    }
}
