﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business
{
    class Context
    {
        private static Model.User _user = new Model.User()
                                        {
                                            Id = 1,
                                            FirstName = "Sunil",
                                            LastName = "Dhummi",
                                            Login = "sunil"
                                        };

        public static Model.User User { get { return _user; } }
    }
}
