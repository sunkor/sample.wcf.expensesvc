﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business
{
    class MySqlPersistence : IExpenseQuery
    {
        private readonly string connectionString = null;

        public MySqlPersistence()
        {
            connectionString = System.Configuration.ConfigurationManager.AppSettings["DbConnection"];
        }
        
        public DataSet FetchCategories()
        {
            DataSet set = new DataSet();
            using (var sqlConnection = new MySqlConnection(connectionString))
            {
                
                var sqlCommand = new MySqlCommand("stpExpenseCategories_PopulateAll");
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = sqlConnection;
                var adapter = new MySqlDataAdapter(sqlCommand);
                adapter.Fill(set);
            }
            return set;
        }

        public long UpdateExpenseItem(long expenseID, DateTime expenseDate, int categoryId, string notes, double amount)
        {
            long expenseId = -1;
            try
            {
                using (var sqlConnection = new MySqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    var sqlCommand = new MySqlCommand("stpExpense_Update");
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    MySqlParameter expenseIdParameter = new MySqlParameter();
                    expenseIdParameter.ParameterName = "expense_id";
                    expenseIdParameter.Value = expenseID;
                    expenseIdParameter.Direction = ParameterDirection.InputOutput;
                    expenseIdParameter.DbType = DbType.Int64;
                    sqlCommand.Parameters.Add(expenseIdParameter);

                    sqlCommand.Parameters.AddWithValue("UserId", Context.User.Id);
                    sqlCommand.Parameters.AddWithValue("ExpenseDate", (DateTime)expenseDate);
                    sqlCommand.Parameters.AddWithValue("CategoryId", categoryId);
                    sqlCommand.Parameters.AddWithValue("Notes", notes);
                    sqlCommand.Parameters.AddWithValue("Amount", amount);

                    sqlCommand.Connection = sqlConnection;
                    if (sqlCommand.ExecuteNonQuery() > 0)
                    {
                        expenseId = (long)expenseIdParameter.Value;
                    }
                }
            }
            catch(Exception)
            {
                expenseId = -1;
            }
            return expenseId;
        }

        public bool DeleteExpenseItem(long expenseID)
        {
            bool bOK = false;
            try
            {
                using (var sqlConnection = new MySqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    int result = -1;

                    var sqlCommand = new MySqlCommand("stpExpense_Delete");
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    MySqlParameter resultParameter = new MySqlParameter();
                    resultParameter.ParameterName = "result";
                    resultParameter.Value = -1;
                    resultParameter.Direction = ParameterDirection.InputOutput;
                    resultParameter.DbType = DbType.Int32;
                    sqlCommand.Parameters.Add(resultParameter);

                    sqlCommand.Parameters.AddWithValue("user_id", Context.User.Id);
                    sqlCommand.Parameters.AddWithValue("expense_id", (long)expenseID);
                    
                    sqlCommand.Connection = sqlConnection;
                    if (sqlCommand.ExecuteNonQuery() > 0)
                    {
                        result = (int)resultParameter.Value;
                    }
                    if(result == 1)
                    {
                        bOK = true;
                    }
                }
            }
            catch (Exception)
            {
                bOK = false;
            }
            return bOK;
        }

        public DataSet GetExpenseItems()
        {
            DataSet set = new DataSet();
            using (var sqlConnection = new MySqlConnection(connectionString))
            {
                var sqlCommand = new MySqlCommand("stpExpense_PopulateAll");
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Parameters.AddWithValue("user_id", Context.User.Id);
                var adapter = new MySqlDataAdapter(sqlCommand);
                adapter.Fill(set);
            }
            return set;
        }

        public DataSet SearchExpenseItems(DateTime? dateFrom, DateTime? dateTo)
        {
            DataSet set = new DataSet();
            using (var sqlConnection = new MySqlConnection(connectionString))
            {
                var sqlCommand = new MySqlCommand("stpExpense_Search");
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Parameters.AddWithValue("user_id", Context.User.Id);
                if (dateFrom != null)
                    sqlCommand.Parameters.AddWithValue("date_from", dateFrom.Value);
                if (dateTo != null)
                    sqlCommand.Parameters.AddWithValue("date_to", dateTo.Value);
                var adapter = new MySqlDataAdapter(sqlCommand);
                adapter.Fill(set);
            }
            return set;
        }

        public DataSet SearchExpenseItemById(long expenseId)
        {
            DataSet set = new DataSet();
            using (var sqlConnection = new MySqlConnection(connectionString))
            {
                var sqlCommand = new MySqlCommand("stpExpense_Populate");
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Parameters.AddWithValue("user_id", Context.User.Id);
                sqlCommand.Parameters.AddWithValue("expense_id", expenseId); 
                var adapter = new MySqlDataAdapter(sqlCommand);
                adapter.Fill(set);
            }
            return set;
        }
    }
}
