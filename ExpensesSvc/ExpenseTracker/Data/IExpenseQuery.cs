﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business
{
    public interface IExpenseQuery
    {
        DataSet FetchCategories();

        long UpdateExpenseItem(long expenseID, DateTime expenseDate, int categoryId, string notes, double amount);
        bool DeleteExpenseItem(long expenseID);

        DataSet GetExpenseItems();
        DataSet SearchExpenseItems(DateTime? dateFrom, DateTime? dateTo);
        DataSet SearchExpenseItemById(long expenseId);
    }
}
