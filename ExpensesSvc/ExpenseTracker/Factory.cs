﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business
{
    public static class Factory
    {
        private static IExpenseQuery _expenseQuery;

        public static IExpenseTracker GetExpenseTracker()
        {
            return new ExpenseTracker(GetExpenseQuery());
        }

        internal static IExpenseQuery GetExpenseQuery()
        {
            if (_expenseQuery == null)
                _expenseQuery = new MySqlPersistence();
            return _expenseQuery;
        }
    }
}
