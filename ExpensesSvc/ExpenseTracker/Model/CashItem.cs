﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Business.Model
{
    class CashItem
    {
        public long ExpenseId { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string Notes { get; set; }
        public double Amount { get; set; }
    }
}
