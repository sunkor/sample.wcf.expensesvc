﻿using System;
namespace ExpenseTracker.Business
{
    public interface IExpenseTracker
    {
        ExpenseCategory[] ExpenseCategories { get; }
                
        long CreateExpenseItem(DateTime expenseDate, int categoryId, string notes, double amount);
        bool TrySaveExpenseItem(long expenseID, DateTime expenseDate, int categoryId, string notes, double amount);
        bool TryDeleteExpenseItem(long expenseId);

        //long CreateExpenseItemAsync(DateTime expenseDate, int categoryId, string notes, decimal amount);
        //bool TrySaveExpenseItemAsync(long expenseID, DateTime expenseDate, int categoryId, string notes, decimal amount);
        //bool TryDeleteExpenseItemAsync(long expenseId);

        string [] GetExpenseItems();
        //string GetExpenseItemsAsync();

        string [] SearchExpenseItems(DateTime? dateFrom, DateTime? dateTo);
        string SearchExpenseItemById(long expenseId);
    }
}

