﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpenseTracker.Business;

namespace ExpenseTrackerConsole
{
    class Program
    {
        class ExpenseCategories
        {
            public static int BILLS = 1;
            public static int FOOD = 2;
            public static int GROCERY = 3;
            public static int TRAVEL = 4;
            public static int ENTERTAIMENT = 5;
        }

        static void Main(string[] args)
        {
            //log4net.Config.XmlConfigurator.Configure();
            
            IExpenseTracker expenseTracker = Factory.GetExpenseTracker();
            var data = expenseTracker.ExpenseCategories;

            //long expenseId = -1;
            //expenseId = expenseTracker.CreateExpenseItem(Convert.ToDateTime("02 June 2016"), ExpenseCategories.BILLS, "Internode data", 61.25);
            //expenseId = expenseTracker.CreateExpenseItem(Convert.ToDateTime("03 June 2016"), ExpenseCategories.GROCERY, "Milk", 2.50);
            //expenseId = expenseTracker.CreateExpenseItem(Convert.ToDateTime("04 June 2016"), ExpenseCategories.TRAVEL, "Opal", 40.00);
            //bool updateResult = expenseTracker.TrySaveExpenseItem(12, Convert.ToDateTime("02 June 2016"), ExpenseCategories.BILLS, "Internode data", 62.25);
            //bool deleteResult = expenseTracker.TryDeleteExpenseItem(11);

            //string [] items = expenseTracker.GetExpenseItems();
            //Console.WriteLine(string.Join(Environment.NewLine, items));

            //string  item = expenseTracker.SearchExpenseItemById(14);
            //Console.WriteLine(item);

            string[] items = expenseTracker.SearchExpenseItems(Convert.ToDateTime("01 June 2016"), Convert.ToDateTime("03 June 2016"));
            Console.WriteLine(string.Join(Environment.NewLine, items));

            Console.WriteLine("Press any key to exit...");
            Console.Read();
        }
    }
}
